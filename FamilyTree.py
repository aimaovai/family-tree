male = (
"Rickard", "Bran", "Hoster", "Ned", "Tywin", "Jaimie", "Tyrion", "Joffrey", "Tommen", "Robert", "Stannis", "Renly",
"Steffon", "Aegon V", "Ormund", "Tytos", "Maekar I","Son1","Son2")
female = ("Arya", "Sansa", "Catelyn", "Lysa", "Minisa", "Cersei", "Myrcella", "Shireen", "Margery", "Joanna", "Rhaelle")

parents = {
    "Ned": ["Rickard"],
    "Arya": ["Ned", "Catelyn"],
    "Sansa": ["Ned", "Catelyn"],
    "Bran": ["Ned", "Catelyn"],
    "Lysa": ["Hoster", "Minisa"],
    "Catelyn": ["Hoster", "Minisa"],
    "Joffrey": ["Robert", "Cersei"],
    "Tommen": ["Robert", "Cersei"],
    "Myrcella": ["Robert", "Cersei"],
    "Shireen": ["Stannis"],
    "Tyrion": ["Tywin", "Joanna"],
    "Jaimie": ["Tywin", "Joanna"],
    "Cersei": ["Tywin", "Joanna"],
    "Robert": ["Steffon"],
    "Stannis": ["Steffon"],
    "Renly": ["Steffon"],
    "Steffon": ["Rhaelle", "Ormund"],
    "Rhaelle": ["Aegon V"],
    "Aegon V": ["Maekar I"],
    "Rhaelle": ["Son1"],
    "Son1": ["Son2"]
}


# print(parents["Joffrey"])
def parent_of(person, other=""):
    if other == "Who" or other == "":
        if person in parents:
            return parents[person]
    else:
        return other in parents[person]


def grandparent_of(person, other=""):
    oldies = []  # list to hold all grandparents
    check = False  # to check for true/false conclusions

    if person in parents:
        for parental in parents[person]:
            if parental in parents:
                oldies.append(parents[parental])
            else:
                oldies.append([])

    if other == "Who" or other == "":
        if person in parents:
            return oldies
        else:
            return []
    else:
        for old in oldies:
            if other in old: check = True
        return check


def cousin_of(person, other=""):
    cousins = []
    check = False

    if other == "Who" or other == "":
        gp = grandparent_of(person)
        for i in gp:
            for p in parents:
                cus_gp = grandparent_of(p)
                for j in cus_gp:
                    if j == i and p != person and p not in sibling_of(person):
                        cousins.append(p)
        return cousins
    else:
        if person in parents and other in parents:
            oldie = grandparent_of(person)
            temp = grandparent_of(other)
            for place in temp:
                for old in oldie:
                    if old == place: check == True
            return check
        else:
            return False


# parent_of("Jeffrey")

def sibling_of(person, other=""):
    if other == "Who" or other == "":
        siblings = []
        for i in parents:
            if parents[person] == parents[i] and i != person:
                siblings.append(i)
        return siblings
    else:
        return parents[person] == parents[other]


def brother_of(person, other=""):
    if other == "Who" or other == "":
        bros = []
        for people in parents:
            if parents[person] == parents[people] and people in male and people != person: bros.append(people)
        return bros
    else:
        return parents[person] == parents[other]


# brother_of("Joffrey")

def sister_of(person, other=""):
    if other == "Who" or other == "":
        sisters = []
        for people in parents:
            if parents[person] == parents[people] and people in female and people != person: sisters.append(people)
        return sisters
    else:
        return parents[person] == parents[other]


# sister_of("Joffrey")

def mother_of(person):
    parent = []
    for p in parent_of(person):
        if p in female:
            parent.append(p)
    return parent


def father_of(person):
    parent = []
    for p in parent_of(person):
        if p in male:
            parent.append(p)
    return parent


def uncle_of_or_aunt_of(person, other=""):
    if other == "Who" or other == "":
        uncles_aunts = []
        for p in parents[person]:
            sibling = sibling_of(p)
            for i in sibling:
                uncles_aunts.append(i)
        return uncles_aunts
    else:
        for parent in parents[person]:
            if sibling_of(parent, other) is True: return True
        return False


def uncle_of(person, other=""):
    if other == "Who" or other == "":
        uncles = []
        for p in parents[person]:
            uncle = brother_of(p)
            for i in uncle:
                uncles.append(i)
        return uncles
    else:
        for parent in parents[person]:
            if sibling_of(parent, other) is True: return True
        return False


def aunt_of(person, other=""):
    if other == "Who" or other == "":
        aunts = []
        for parent in parents[person]:
            aunt = sister_of(parent)
            for i in aunt:
                aunts.append(i)
        return aunts
    else:
        for parent in parents[person]:
            if sibling_of(parent, other) is True: return True
        return False


def ancestor_of(person, other=""):
    temp = []
    ps = []
    if other == "" or other == "Who":  # if the requested output is for a list
        p = parent_of(person)
        if p == None:
            return None
        else:
            for i in p:
                ps.append(i)
        for i in p:
            temp = ancestor_of(i)
            if temp != None:
                for j in temp:
                    ps.append(j)
        return ps
    else:  # else run the boolean statements
        if person in parents and other in parents:
            if other in parents[person]:
                return True
            else:
                for p in parents[person]:
                    if (ancestor_of(parents[person][0], other) == True): return True
        return False
